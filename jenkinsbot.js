"use strict";
var config = require("./config.json");
var irc = require("irc");
var chance = require("chance").Chance();

var commands = {
	"(hurt|hurts)*pee": function(){
		return "Maybe you should see a doctor?";
	},
	"pyramid": function(){
		return "▲";
	},
	"rps": function(from, to, text){
		var words = text.split(" ");
		var userChoice = words.pop();
		var botChoice = chance.pick(["rock","paper","scissors"]);

		if(userChoice === botChoice){
			return "Tie! We both picked "+botChoice+".";
		}else if(userChoice === "rock"){
			if(botChoice === "paper") return "You lose! I picked paper.";
			if(botChoice === "scissors") return "You win! I picked scissors.";
		}else if(userChoice === "paper"){
			if(botChoice === "rock") return "You win! I picked rock.";
			if(botChoice === "scissors") return "You lose! I picked scissors.";
		}else if(userChoice === "scissors"){
			if(botChoice === "rock") return "You lose! I picked rock.";
			if(botChoice === "paper") return "You win! I picked paper.";
		}else{
			return "That's not a valid move in rock paper scissors!";
		}
	},
	"rock paper scissors": function(from, to, text){
		return this["rps"](from, to, text);
	},
	"praise": function(from, to, text){
		var words = text.split(" ");
		var whoToPraise = words.pop();
		var chain = "is";
		if(whoToPraise === "me"){
			whoToPraise = from;
		}else if(whoToPraise === "you" || whoToPraise === "yourself"){
			whoToPraise = "I";
			chain = "am";
		}
		var adjectives = [
			"beautiful", "fabulous", "sexy",
			"sharp", "lovely", "handsome",
			"cute"
		];
		var adverbs = [
			"simply", "extra", "absolutely",
			"utterly", "perfectly", "quite",
			"thoroughly"
		];

		return whoToPraise + " " + chain + " looking " + chance.pick(adverbs) +  " " + chance.pick(adjectives) + " today!";
	},
	"thanks|ty|thank you": function(){
		return "You're welcome!";
	}
};

var parseMessage = function(from, to, text, message){
	for(var regex in commands){
		if((new RegExp(regex)).test(text)){
			console.log("# Command match #");
			return commands[regex](from, to, text, message);
		}
	}
	return false;
};

var jenkinsbot = new irc.Client(config.server, config.name, {
	//sasl: true,
	nick: config.name,
	userName: config.name,
	realName: config.name,
	password: config.password,
	port: config.port,
	showErrors: true,
	channels: config.channels,
	stripColors: true
});

jenkinsbot.addListener("join", function(channel, who){
	// Welcome to the channel!
	console.log(who + " joined " + channel);
});

jenkinsbot.addListener("message", function(from, to, text, message){
	console.log(new Date().getTime(), from, text);

	if(new RegExp("^("+config.prefix+"|"+config.name+")[, \s]").test(text)){
		console.log("# Mentioned #");
		var returnMessage = parseMessage(from, to, text, message);
		if(returnMessage){
			var spaces = "";
			for(var i=0; i<new Date().getSeconds(); i++){
				spaces = spaces + " ";
			}
			jenkinsbot.say(message.args[0], returnMessage+spaces);
		}
	}

	
	
});

jenkinsbot.addListener("error", function(e){
	console.log(e);
});